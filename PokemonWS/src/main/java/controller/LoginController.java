package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;

import facade.LoginFacade;
import jsonModel.LoginJSONModel;
import model.LoginModel;

@WebServlet(urlPatterns = {"/", "/Login"})
public class LoginController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("index.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String servletPath = request.getServletPath();

		if (servletPath.equals("/Login")) {
			ObjectMapper objectMapper = new ObjectMapper();
			LoginModel loginModel = new LoginModel();
			LoginFacade loginFacade = new LoginFacade();
			LoginJSONModel loginJSONModel = new LoginJSONModel();

			loginModel.setUsername(request.getParameter("username"));
			loginModel.setPassword(request.getParameter("password"));
			loginFacade.verify(loginModel, loginJSONModel);

			response.setContentType("application/json; charset=UTF-8;");
			response.getWriter().println(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(loginJSONModel));
		} else {
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}
	}
}