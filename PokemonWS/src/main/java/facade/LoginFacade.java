package facade;

import entity.LoginEntity;
import jsonModel.LoginJSONModel;
import model.LoginModel;
import persist.LoginPersist;

public class LoginFacade {

	public void verify(LoginModel loginModel, LoginJSONModel loginJSONModel){
		LoginEntity loginEntity = new LoginEntity();
		LoginPersist loginPersist = new LoginPersist();
		
		loginEntity.setUsername(loginModel.getUsername());
		loginPersist.selectPassword(loginEntity);
		if(loginEntity.getPassword() != null){
			if(loginModel.getPassword().equals(loginEntity.getPassword())){
				loginJSONModel.setStatusLogin(true);
				loginPersist.insertIntoLog(loginEntity);
			} else {
				loginJSONModel.setStatusLogin(false);
			}
		} else {
			loginJSONModel.setStatusLogin(false);
		}
	}
}