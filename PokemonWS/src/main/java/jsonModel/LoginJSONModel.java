package jsonModel;

public class LoginJSONModel {

	private boolean statusLogin;

	public boolean isStatusLogin() {
		return statusLogin;
	}

	public void setStatusLogin(boolean statusLogin) {
		this.statusLogin = statusLogin;
	}
}