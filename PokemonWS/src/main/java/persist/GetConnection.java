package persist;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class GetConnection {

	private String DB_DRIVER = "oracle.jdbc.driver.OracleDriver";
	private String DB_CONNECTION = "jdbc:oracle:thin:@PNLNP:1521:ORCL";
	private String DB_USER = "PokemonWS";
	private String DB_PASSWORD = "pokemonws";

	public Connection getDBConnection() throws ClassNotFoundException, SQLException{
		Connection dbConnection = null;
		
		Class.forName(DB_DRIVER);
		dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
	
		return dbConnection;
	}
}