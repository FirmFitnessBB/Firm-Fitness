package persist;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import entity.LoginEntity;

public class LoginPersist {

	private GetConnection getConnection = new GetConnection();
	
	public void selectPassword(LoginEntity loginEntity){
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String selectPassword = "SELECT USER_PASSWORD FROM WS_USER WHERE USER_NAME = ?";				
		
		try {
			dbConnection = getConnection.getDBConnection();
			preparedStatement = dbConnection.prepareStatement(selectPassword);
			preparedStatement.setString(1, loginEntity.getUsername());
			resultSet = preparedStatement.executeQuery();
			if(resultSet.next()){
				loginEntity.setPassword(resultSet.getString("PASSWORD"));
			} else {
				loginEntity.setPassword(null);
			}
		} catch (ClassNotFoundException e) {
			loginEntity.setErrorStatus(e.getMessage());
		} catch (SQLException e) {
			loginEntity.setErrorStatus(e.getMessage());
		} catch (NullPointerException e) {
			loginEntity.setErrorStatus(e.getMessage());
		} finally {
			try {
				preparedStatement.close();
				dbConnection.close();
			} catch (SQLException e) {
				loginEntity.setErrorStatus(e.getMessage());
			}
		}
	}
	
	public void insertIntoLog(LoginEntity loginEntity){
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;
		String insertLog = "INSERT INTO LOG (USER_ID, LOGIN_DATETIME) VALUES "
							  + "SELECT USER_ID, SYSDATE FROM WS_USER WHERE USER_NAME = ?";				
		
		try {
			dbConnection = getConnection.getDBConnection();
			preparedStatement = dbConnection.prepareStatement(insertLog);
			preparedStatement.setString(1, loginEntity.getUsername());
			preparedStatement.executeQuery();
		} catch (ClassNotFoundException e) {
			loginEntity.setErrorStatus(e.getMessage());
		} catch (SQLException e) {
			loginEntity.setErrorStatus(e.getMessage());
		} catch (NullPointerException e) {
			loginEntity.setErrorStatus(e.getMessage());
		} finally {
			try {
				preparedStatement.close();
				dbConnection.close();
			} catch (SQLException e) {
				loginEntity.setErrorStatus(e.getMessage());
			}
		}
	}
}