<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>Pokemon WS</title>
<meta charset="utf-8">
<link type="text/css" rel="stylesheet"
	href="<c:url value="/resources/styles/bootstrap.min.css" />" />
<link type="text/css" rel="stylesheet"
	href="<c:url value="/resources/styles/login.css" />" />
</head>
<body>
	<div class="login-page">
		<div class="form">
			<form class="login-form" method="POST">
				<p id="head">Pokemon Workshop Login</p>
				<div class="center">
					<font color="red" id="loginFail"></font>
				</div>
				<input id="username" name="username" type="text"
					placeholder="username" /> <input id="password" name="password"
					type="password" placeholder="password" />
				<div class="center">
					<font color="grey" id="checkUsernameOrPassword"></font>
				</div>
				<br>
				<div>
					<button type='button' onclick="window.location.href='Register'">Register</button>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<button type='button' onclick="login()">Login</button>
				</div>
			</form>
		</div>
	</div>
</body>
<script type="text/javascript"
	src="<c:url value="/resources/javascript/login.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/script/jquery.min.js" />"></script>
</html>