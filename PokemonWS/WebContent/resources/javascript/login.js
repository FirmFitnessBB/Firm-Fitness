function login(){
	$.ajax({
		type : 'POST',
		url : "Login",
		data : {
			"username" : $('#username').val(),
			"password" : $('#username').val()
		},
		async : false,
		success : function(result) {
			if(result.loginStatus){
				window.location.href='PokemonMain';
			} else {
				$('#loginFail').html("Username or Password is incorrect.");
				$('#checkUsernameOrPassword').html("");
				$('#password').val("");
			}
		},
		error : function(jqXHR, testStatus, errorThrown) {

		},
	});
	return false;
}

function getTimeParam() {
	var time = "&time=" + new Date().getTime();
	return time;
}